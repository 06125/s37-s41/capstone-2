const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/userRoutes");
const adminRoutes = require("./routes/adminRoutes");
const productRoutes = require("./routes/productRoutes");

const app = express();
const PORT = process.env.PORT || 3000;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

mongoose
  .connect(
    "mongodb+srv://eclespinosa:emilmongo@cluster0.vtyel.mongodb.net/e_commerce?retryWrites=true&w=majority",
    {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    }
  )
  .then(() => console.log("Connected to database."))
  .catch((error) => console.log(error));

app.use("/", userRoutes);
app.use("/products", productRoutes);
app.use("/admin", adminRoutes);

app.use("*", (req, res) => res.send("Page not found."));

app.listen(PORT, () => console.log(`Server running at port ${PORT}.`));

/* https://mighty-thicket-59478.herokuapp.com/ | https://git.heroku.com/mighty-thicket-59478.git */
