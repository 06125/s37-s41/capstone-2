const express = require("express");
const router = express.Router();
const userController = require("./../controllers/userControllers");
const auth = require("./../auth");

router.post("/register", (req, res) => {
  userController.register(req.body).then((result) => res.send(result));
});

router.post("/register/check-credentials", (req, res) => {
  userController.checkEmail(req.body.email).then((result) => res.send(result));
});

router.post("/login", (req, res) => {
  userController.login(req.body).then((result) => res.send(result));
});

router.get("/users/me", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  userController.getDetails(userData.id).then((result) => res.send(result));
});

router.put("/users/me/settings", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  userController
    .editDetails(userData.id, req.body)
    .then((result) => res.send(result));
});

router.get("/users/cart", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  userController.cartOrders(userData.id).then((result) => res.send(result));
});

router.post("/users/cart/check-out", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  if (!userData.isAdmin) {
    userController
      .checkOutOrder(userData.id)
      .then((result) => res.send(result));
  } else {
    res.send("You're an admin, why are you creating an order?");
  }
});

router.get("/users/orders", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  userController.viewMyOrders(userData.id).then((result) => res.send(result));
});

module.exports = router;
