const express = require("express");
const router = express.Router();
const adminController = require("./../controllers/adminControllers");
const auth = require("./../auth");

router.put("/:userId/set-admin", auth.verifyAdmin, (req, res) => {
  adminController
    .setAdmin(req.params.userId)
    .then((result) => res.send(result));
});

router.post("/products/add", auth.verifyAdmin, (req, res) => {
  adminController.addProduct(req.body).then((result) => res.send(result));
});

router.get("/products/all", auth.verifyAdmin, (req, res) => {
  adminController.allProducts().then((result) => res.send(result));
});

router.put("/products/:productId/edit", auth.verifyAdmin, (req, res) => {
  adminController
    .editProduct(req.params.productId, req.body)
    .then((result) => res.send(result));
});

router.put("/products/:productId/archive", auth.verifyAdmin, (req, res) => {
  adminController
    .editProduct(req.params.productId, { isActive: false })
    .then((result) => res.send(result));
});

router.put("/products/:productId/unarchive", auth.verifyAdmin, (req, res) => {
  adminController
    .editProduct(req.params.productId, { isActive: true })
    .then((result) => res.send(result));
});

router.delete("/products/:productsId/delete", auth.verifyAdmin, (req, res) => {
  adminController
    .deleteProduct(req.params.productsId)
    .then((result) => res.send(result));
});

router.get("/orders", auth.verifyAdmin, (req, res) => {
  adminController.viewOrders().then((result) => res.send(result));
});

module.exports = router;
