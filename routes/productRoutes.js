const express = require("express");
const router = express.Router();
const productController = require("./../controllers/productControllers");
const auth = require("./../auth");

router.get("/", auth.verify, (req, res) => {
  productController.getActive().then((result) => res.send(result));
});

router.get("/search", auth.verify, (req, res) => {
  let { q } = req.query;
  productController.searchProducts(q).then((result) => res.send(result));
});

router.get("/:productId", auth.verify, (req, res) => {
  productController
    .getOneProduct(req.params.productId)
    .then((result) => res.send(result));
});

router.put("/:productId/addToCart", auth.verify, (req, res) => {
  let userData = auth.decode(req.headers.authorization);
  productController
    .addToCart(userData.id, req.params.productId)
    .then((result) => res.send(result));
});

router.post("/:productId/check-out", auth.verify, (req, res) => {
  let userData = auth.decode(req.headers.authorization);
  productController;
  productController
    .buyNow(userData.id, req.params.productId)
    .then((result) => res.send(result));
});

module.exports = router;
