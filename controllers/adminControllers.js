const User = require("./../models/User");
const Product = require("./../models/Product");
const Order = require("../models/Order");

module.exports.allProducts = () => {
  return Product.find({}).then((result) => (result ? result : false));
};

module.exports.setAdmin = (userId) => {
  return User.findByIdAndUpdate(userId, { isAdmin: true }).then((result) =>
    result ? true : false
  );
};

module.exports.addProduct = (productInfo) => {
  let newProduct = new Product(productInfo);
  return newProduct.save().then((result, error) => (error ? error : true));
};

module.exports.editProduct = (productId, updates) => {
  return Product.findByIdAndUpdate(productId, updates).then((result) =>
    result ? true : false
  );
};

module.exports.deleteProduct = (productId) => {
  return Product.findByIdAndRemove(productId).then((result) =>
    result ? true : false
  );
};

module.exports.viewOrders = () => {
  return Order.find({}).then((result) => (result ? result : false));
};
