const Order = require("../models/Order");
const Product = require("./../models/Product");
const User = require("./../models/User");

module.exports.getActive = () => {
  return Product.find({ isActive: true }).then((result) =>
    result ? result : false
  );
};

module.exports.getOneProduct = (id) => {
  return Product.findById(id).then((result) => (result ? result : false));
};

module.exports.searchProducts = (query) => {
  return Product.find({ name: { $regex: query, $options: "i" } }).then(
    (result) => {
      if (result && result.length !== 0) {
        return result;
      } else {
        return "Nothing matches your search.";
      }
    }
  );
};

module.exports.addToCart = async (userId, productId) => {
  let price;
  const gettingPriceStatus = await Product.findById(productId).then((item) => {
    if (!item) return false;

    price = item.price;
    return true;
  });

  const addingToCartStatus = await User.findById(userId).then((user) => {
    if (!user) return false;

    user.cartItems.push({ productId, price });
    return user.save().then((course, error) => (error ? false : true));
  });

  if (gettingPriceStatus && addingToCartStatus) {
    return true;
  } else {
    return false;
  }
};

module.exports.buyNow = async (userId, productId) => {
  let stocks;
  let stockStatus;

  const creatingOrderStatus = await Product.findById(productId).then((item) => {
    stocks = item.stocks;

    let newOrder = new Order({
      totalAmount: item.price,
      buyerId: userId,
      products: [{ productId }],
    });

    return newOrder.save().then((result) => (result ? true : false));
  });

  if (stocks > 1) {
    stockStatus = await Product.findByIdAndUpdate(productId, {
      stocks: stocks - 1,
    }).then((result) => (result ? true : false));
  } else {
    stockStatus = await Product.findByIdAndUpdate(productId, {
      isActive: false,
      stocks: 0,
    }).then((result) => (result ? true : false));
  }

  if (creatingOrderStatus && stockStatus) {
    return true;
  } else {
    return false;
  }
};
