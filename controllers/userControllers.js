const bcrypt = require("bcrypt");
const auth = require("./../auth");
const User = require("./../models/User");
const Product = require("./../models/Product");
const Order = require("./../models/Order");

module.exports.register = (reqBody) => {
  let { firstName, lastName, email, password } = reqBody;

  let newUser = new User({
    firstName,
    lastName,
    email,
    password: bcrypt.hashSync(password, 10),
  });

  return newUser.save().then((result, error) => (error ? error : true));
};

module.exports.checkEmail = (email) => {
  return User.find({ email }).then((result) =>
    result.length === 0 ? true : false
  );
};

module.exports.login = (reqBody) => {
  let { email, password } = reqBody;
  return User.findOne({ email }).then((result) => {
    if (bcrypt.compareSync(password, result?.password || "")) {
      return { access: auth.createAccessToken(result.toObject()) };
    } else {
      return false;
    }
  });
};

module.exports.getDetails = (userId) => {
  return User.findById(userId, "-password").then((result) =>
    result ? result : false
  );
};

module.exports.editDetails = (userId, updates) => {
  return User.findByIdAndUpdate(userId, updates).then((result) =>
    result ? true : false
  );
};

module.exports.cartOrders = (userId) => {
  return User.findById(userId).then((result) =>
    result ? result.cartItems : false
  );
};

module.exports.checkOutOrder = (userId) => {
  return User.findById(userId).then((user) => {
    if (user.cartItems.length === 0) return "No item in the cart.";

    let totalAmount = 0;
    let products = [];

    user.cartItems.forEach((item) => {
      totalAmount += item.price;
      products.push({ productId: item.productId });
    });

    let newOrder = new Order({
      totalAmount,
      buyerId: userId,
      products,
    });

    user.cartItems = [];
    const orderStatus = newOrder
      .save()
      .then((result) => (result ? true : false));

    if (orderStatus) {
      return user.save().then((result) => (result ? true : false));
    } else {
      return false;
    }
  });
};

module.exports.viewMyOrders = (userId) => {
  return Order.find({ buyerId: userId }).then((result) =>
    result ? result : false
  );
};
