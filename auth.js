const jwt = require("jsonwebtoken");
const secret = "e-commerce";

module.exports = {
  createAccessToken: (user) => {
    const data = {
      id: user._id,
      email: user.email,
      isAdmin: user.isAdmin,
    };

    return jwt.sign(data, secret);
  },

  verify: (req, res, next) => {
    const token = req.headers.authorization?.split(" ")[1];

    if (token) {
      return jwt.verify(token, secret, (error, data) => {
        if (error) {
          return res.send({ auth: "failed. Not a user." });
        } else {
          next();
        }
      });
    } else {
      return res.send({ auth: "failed. Not a user." });
    }
  },

  verifyAdmin: (req, res, next) => {
    const token = req.headers.authorization?.split(" ")[1];

    if (token) {
      return jwt.verify(token, secret, (error, data) => {
        if (error || !data.isAdmin) {
          return res.send({ auth: "failed. Not an admin." });
        } else {
          next();
        }
      });
    } else {
      return res.send({ auth: "failed. Not an admin." });
    }
  },

  decode: (token) => {
    if (token) {
      token = token?.split(" ")[1];
      return jwt.decode(token, { complete: true }).payload;
    }
  },
};
